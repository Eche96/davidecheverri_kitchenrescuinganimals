﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Dish: MonoBehaviour
{
    private List<Ingredient> recipe = new List<Ingredient>();
    public Action<GameObject> SendFood;

    private void Awake()
    {
        GetComponentInChildren<SensorDish>().Ingredente += OnIngredente;
    }
    private void OnIngredente(GameObject _target, bool enterOrExit)
    {
        if (enterOrExit)
        {
            EnterFood(_target);
        }
        else
        {
            ExitFood(_target);
        }
    }

    public void NewRecipe(List<Ingredient> _recipe)
    {
        recipe.Clear();
        recipe = _recipe;
        for (int i = 0; i < recipe.Count; i++)
        {
            Debug.Log(recipe[i].name + recipe[i].stateCut);
        }
    }

    private void EnterFood(GameObject FoodEnter)
    {
        for (int i = 0; i < recipe.Count; i++)
        {
            if (recipe[i].name == FoodEnter.name)
            {
                Debug.Log("Compatible");
                EvaluateStates(i, FoodEnter);
                break;
            }
        }
    }

    private void ExitFood(GameObject FoodEnter)
    {

        if(FoodEnter.GetComponent<Food_States_Base>().InRecipe == true)
        {
            Ingredient Enteringredient = new Ingredient();
            Enteringredient.name = FoodEnter.name;
            Enteringredient.cokedState = FoodEnter.GetComponent<Food_States_Base>().Coked_States.ToString();
            Enteringredient.stateCut = FoodEnter.GetComponent<Food_States_Base>().Cut_States.ToString();
            recipe.Add(Enteringredient);
            FoodEnter.GetComponent<Food_States_Base>().InRecipe = false;
            Debug.Log("ExitFood " + recipe.Count);
        }

    }

    private void EvaluateStates(int ingredientList, GameObject _target)
    {
        if (recipe[ingredientList].stateCut == _target.GetComponent<Food_States_Base>().Cut_States.ToString() && recipe[ingredientList].cokedState == _target.GetComponent<Food_States_Base>().Coked_States.ToString())
        {
            _target.GetComponent<Food_States_Base>().InRecipe = true;
            recipe.RemoveAt(ingredientList);
            Debug.Log("EnterFood" + recipe.Count);
            CheckCompleteRecipe();
        }
        else
        {
            Debug.Log("No esta bien");
        }
    }

    private void CheckCompleteRecipe()
    {
        if(recipe.Count == 0)
        {
            Debug.Log("Se completo la receta");
        }
    }
}
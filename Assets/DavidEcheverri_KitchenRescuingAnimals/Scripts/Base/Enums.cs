﻿public enum Cut_States
{
    Whole = 0,
    Cut_Half = 1,
    Cut_Quarters = 2,
    Smoothie = 3,
    Grated = 4
}

public enum Coked_States
{
    Raw = 0,
    Fry = 1,
    Boil = 2,
    Burned = 3,
}

public enum Implement_Use
{
    Fry = 1,
    Boil = 2,
    Bale = 3,
    Cut = 4,
    Smoothie = 5
}

public enum Game_States
{
    Main,
    Tutorial,
    Selec_Recipe,
    Gameplay,
    Score
}

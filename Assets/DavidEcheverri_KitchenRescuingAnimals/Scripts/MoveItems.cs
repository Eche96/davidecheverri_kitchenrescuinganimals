﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveItems : MonoBehaviour
{
    private Vector3 mOffSet;
    private float mZCoord;
    private RaycastHit hit;
    private GameObject takeGameobject = null;
    private float distance = 3f;
    public LayerMask activeLayers;


    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction, Color.green);
        if(Physics.Raycast(ray,out hit, Mathf.Infinity,activeLayers))
        {
            Debug.Log(hit.transform.name);
        }



        if (Input.GetButton("Fire1"))
        {
            if(takeGameobject == null)
            {
                if(hit.transform.tag == "Food")
                {
                    takeGameobject = hit.transform.gameObject;
                }
                
            }
            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y,Camera.main.WorldToScreenPoint(takeGameobject.transform.position).z);
            Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);
            takeGameobject.transform.position = objPosition;
        }

        if (Input.GetButtonUp("Fire1"))
        {
            takeGameobject = null;
        }
    }

}

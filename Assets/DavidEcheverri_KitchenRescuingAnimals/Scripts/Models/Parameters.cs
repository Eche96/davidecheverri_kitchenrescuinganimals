﻿
public class Parameters
{
    public float fryAmount;
    public float pointFry;
    public float fryTimeAmount;
    public float burnedpointfry;
    public float delayActionFry;
    public float boilAmount;
    public float pointBoil;
    public float boilTimeAmount;
    public float burnedpointBoil;
    public float delayActionBoil;
    public Cut_States cutStateOrigen;
}
